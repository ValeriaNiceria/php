<?php

$suaIdade = 25;

$idadeCrianca = 12;
$idadeMaior = 18;
$idadeMelhor = 65;

if($suaIdade < $idadeCrianca){
    echo "Você é uma criança!";
}elseif($suaIdade < $idadeMaior){
    echo "Você é um adolescente!";
}elseif($suaIdade < $idadeMelhor){
    echo "Você é um adulto!";
}else{
    echo "Você está na sua melhor idade!";
}    

echo "<br/>";

//Operador ternário

echo ($suaIdade >= $idadeMaior) ? "Você é Maior de Idade!" : "Você é menor de idade!";

?>