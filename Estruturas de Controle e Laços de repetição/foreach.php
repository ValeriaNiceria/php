<?php

$frutas = array(
    'Orange', 
    'Banana', 
    'Lemon', 
    'Apple'
);

foreach($frutas as $fruta){
    echo "Fruta: ".$fruta. "<br/>";
}

$meses = array(
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro'
);

foreach($meses as $index => $mes){
    echo $index .": ".$mes ."<br/>";
}

?> 