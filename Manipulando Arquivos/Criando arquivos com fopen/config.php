<?php

//Registra a função dada como implementação de __autoload()
spl_autoload_register(function($className){
    //DIRECTORY_SEPARATOR é uma constante pré-definida(/)
    $fileName = "class".DIRECTORY_SEPARATOR.$className.".php";
    if(file_exists(($fileName))){
        require_once($fileName);
    }
});

?>