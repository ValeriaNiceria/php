<?php

require_once("config.php");

$sql = new Sql();

$clientes = $sql->select("SELECT * FROM clientes ORDER BY id_cliente");

$headers = array();

foreach($clientes[0] as $key => $value){
    array_push($headers, ucfirst($key));
}

$file = fopen("usuarios.csv", "w+");

fwrite($file, implode(",",$headers) . "\r\n"); //Adicionado o cabeçalho

//Pegando os dados 
foreach($clientes as $row){

    $data = array();

    foreach($row as $key => $value){

        array_push($data, $value);

    }//End foreach de coluna

    //Adicionado os dados ao arquivo
    fwrite($file, implode(",", $data) . "\r\n");

}//End foreach de linha

fclose($file);

?>