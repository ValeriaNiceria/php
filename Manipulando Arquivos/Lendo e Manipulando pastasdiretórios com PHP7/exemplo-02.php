<?php

//Verifica o que tem dentro do diretório
$imagens = scandir("imagens");

$data = array();

foreach($imagens as $imagem){

    if(!in_array($imagem, array(".", ".."))){ //in_array -> faz uma busca dentro do array
        
        $filename = "imagens" . DIRECTORY_SEPARATOR . $imagem;

        $info = pathinfo($filename); //Pega as informações do arquivo

        $info["size"] =  filesize($filename); //Tamanho do arquivo em bytes
        $info["modified"] = date("d/m/Y H:i:s",filemtime($filename)); //Data de modificação do arquivo
        $info["url"] = "ttp://localhost/php/Manipulando Arquivos/Lendo e Manipulando pastasdiretórios com PHP7/".str_replace("\\", "/", $filename);
        
        array_push($data, $info);

    }

}

echo json_encode($data);

?> 